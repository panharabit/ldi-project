Rails.application.routes.draw do
  root "homes#index"
  get "detail" => "homes#detail"
  get "cart" => "homes#cart"
  get "about" => "homes#about"
  get "contact" => "homes#contact"
  get "category" => "homes#category"
  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
end
